<?php

//Se inicia la sesion.
session_start();




//Se incluye archivo que contiene la conexion a la base de datos
require_once 'db.php';

//Sentancia Switch encargada de llamar a las funciones PHP de este archivo segun el valor de la variable task.
$task  = filter_input(INPUT_POST, 'task');
global $Conexion;


switch ($task){

//Segun el valor de la variable es a la funcion que manda llamar.
case 'validUser':
    $user = (filter_input(INPUT_POST, 'user') ? $_POST['user'] : 0);
    $pass = (filter_input(INPUT_POST, 'password') ? $_POST['password'] : 0);

    validUser($user,$pass);
    break;
  
    case 'registrarUsuario':
        $user = (filter_input(INPUT_POST, 'user') ? $_POST['user'] : 0);
        $pass = (filter_input(INPUT_POST, 'password') ? $_POST['password'] : 0);
        $userEmail = (filter_input(INPUT_POST, 'userEmail') ? $_POST['userEmail'] : 0);
    
        registrarUsuario($user,$pass,$userEmail);
        break;
          
        case 'changePass':
            $user = (filter_input(INPUT_POST, 'user') ? $_POST['user'] : 0);
            $pass = (filter_input(INPUT_POST, 'password') ? $_POST['password'] : 0);
        
            changePass($user,$pass);
            break;    
        


            case 'closeSession':
                closeSession();
                break;
        
    

}




/**
 * Funcion que realiza la conexion a la bd mysql
 * @return type
 */
function getHandlerDB106() {
    global $HOST;
    global $PORT;
    global $DATAB;
    global $US;
    global $PASS;
    
    
        try {
            $db = new PDO("mysql:host=$HOST;port=$PORT;dbname=$DATAB", $US, $PASS);
                // mysqli_set_charset($db, 'utf8'); //linea a colocar
    
            return($db);
        } catch (PDOException $e) {
            echo $e;
            return null;
        }
    }







    //Función para validar el usuario
function validUser($user, $pass){
    global $EsquemaGral;
    $authenticated = 'no';
  
    $ObjConectar = getHandlerDB106();
    if ($ObjConectar)
    {
      $query = "SELECT * from testWebDeveloper.users where
       emailUser = '$user' and passwordUser ='$pass'";
  
      $result = $ObjConectar->query($query);
      $data = $result->fetchAll(PDO::FETCH_ASSOC);
  
      if($result and count($data)>0){
        $authenticated = 'si';
        $_SESSION['authenticated'] = $authenticated;
        $_SESSION['idus'] = $data[0]['idUser']; 
 
  
        $response = 1;
       
      
      }else{
        //usuario no encontrado
        $response = 0;
      }
    }
    else{
      //fallo en la conexion
      $response = 2;       
    }
  
      //Se imprime la respuesta de la funcion.
    echo $response;

}





function registrarUsuario($user, $pass, $userEmail){



    $ObjConectar = getHandlerDB106();
  if ($ObjConectar)
  {

        $queryRegistro = " INSERT INTO testWebDeveloper.users (nameUser,emailUser,passwordUser) 
         VALUES ('".$user."','".$userEmail."','".$pass."');";
    

            $stmt= $ObjConectar->prepare($queryRegistro);
            

            if($stmt->execute()){

                
                            $query = "SELECT * from testWebDeveloper.users where
                                emailUser = '$userEmail' and passwordUser ='$pass'";
                            
                                $result = $ObjConectar->query($query);
                                $data = $result->fetchAll(PDO::FETCH_ASSOC);
                            
                                if($result and count($data)>0){
                                    $authenticated = 'si';
                                    $_SESSION['authenticated'] = $authenticated;
                                    $_SESSION['idus'] = $data[0]['idUser']; 

                                   // $response = $_SESSION['idus'];
                                    $response = 1;
                                }else{

                                    //fallo en la conexion
                                    $response = 2; 
                                }
 
  
        
       

            }else{
                //fallo en la conexion
            $response = 2; 
            }


} else{
    //fallo en la conexion
    $response = 2;       
  }



   //Se imprime la respuesta de la funcion.
    echo $response;

    
}   







function changePass($user,$pass){



    $ObjConectar = getHandlerDB106();
  if ($ObjConectar)
  {

        $queryUpdate = "UPDATE testWebDeveloper.users SET passwordUser= '".$pass."' WHERE idUser=$user;";
    

            $stmt= $ObjConectar->prepare($queryUpdate);
            

            if($stmt->execute()){

                //fallo en la conexion
            $response = 1; 

            }else{
                //fallo en la conexion
            $response = 2; 
            }


} else{
    //fallo en la conexion
    $response = 2;       
  }



   //Se imprime la respuesta de la funcion.
    echo $response;

    
}   






/**
* Funcion encargada de cerrar la sesion del usuario
*
*/
function closeSession()
{
    $_SESSION = array();
    session_destroy();
}



?>